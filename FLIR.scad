// FLIR infrared camera
// Written by Ubuntourist <dc.loco@gmail.com> 2016.11.30 (kjc)

// Digital calipers say the "bounding cube" of the FLIR, sans
// curvature, is:
//
// height = 32.99 mm
// width  = 64.75 mm
// depth   = 18.53 mm tapering to 11.98 mm
//

rotate(a=[0,90,0])
translate([-9.265, 0, 0])
union() {
  intersection() {
    cube([18.53, 64.75, 32.99], true);
    translate([54, 0, 0])
    sphere(64.75, $fn=200);
  };
  rotate(a=[0, 90, 0])
  translate([0, 0, -0.5])
  difference() {
    cylinder(19, 11.68, 11.68, true, $fn=200);
    translate([0, 6, 0])
    cylinder(30,  4.5,   4.5,  true, $fn=200);
    translate([0, -6, 0])
    cylinder(30,  4.5,   4.5,  true, $fn=200);
  };
};
